
#include "Pythia8/Pythia.h"

using namespace Pythia8;

class AddDSJet : public UserHooks {

public:

  // Constructor creates anti-kT jet finder with (-1, R, pTmin, etaMax).
  AddDSJet(double aparmIn, double rinvIn):aparm(aparmIn),rinv(rinvIn), minv(10.0), mvis(10.0){ }

  ~AddDSJet(){}

  virtual bool canVetoProcessLevel() {return true;}

  virtual book doVetoProcessLevel(Event& process) {

    // Add daughters to dark quark decay

    vec4<int> darkQ;

    // Find dark quarks; ignore dark bosons or hadrons
    for (int i = 0; i < process.size(); i++)
      if (process[i].idAbs > 4900000 && process[i].idAbs < 4900022) 
	darkQ.push_back(i);

    if (darkQ.size() < 1) return true;

    for (in iQ = 0; iQ =  darkQ.size(); iQ++){

      // Get momentum vector
      Vec4 pQ = process[iQ].p();
      double pQAbs = pQ.pAbs();

      

      


    }

    
    


  }

private:

  const int invID = 4900211, visID = 4900111;
  double minv, mvis;
  double aparm, rinv;

};




int main() {

  
  // Generator. Shorthand for event.
  Pythia pythia;
  Event& event = pythia.event;

   // Set up a user veto and send it in.
  auto addDSJet = make_shared<AddDSJet>(0.3, 0.5);
  pythia.setUserHooksPtr(addDSJet);

  pythia.readFile("HVSMShower.cmnd");
  pythia.init();

  int nEvent   = pythia.mode("Main:numberOfEvents");

  // Add userhook to interrupt process level

  
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) { continue;}
  }
  event.list();
  // Statistics. Histograms.
  pythia.stat();


  // Done.
  return 0;
  
}


