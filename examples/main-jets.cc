// main72.cc is a part of the PYTHIA event generator.
// Copyright (C) 2022 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; fastjet; slowjet; jet finding;

// This is a simple test program.
// It compares SlowJet, FJcore and FastJet, showing that they
// find the same jets.

#include "Pythia8/Pythia.h"

// The FastJet3.h header enables automatic initialisation of
// fastjet::PseudoJet objects from Pythia8 Particle and Vec4 objects,
// as well as advanced features such as access to (a copy of)
// the original Pythia 8 Particle directly from the PseudoJet,
// and fastjet selectors that make use of the Particle properties.
// See the extensive comments in the header file for further details
// and examples.
#include "fjcore.hh"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"

using namespace Pythia8;

int main() {

  double pTMin = 20.0;  // Minimum jet pT
  double etaMax  = 5.0;    // Pseudorapidity range of detector.

  // Generator. Shorthand for event.
  Pythia pythia;
  Event& event = pythia.event;

  // Set up the ROOT TFile
  TFile *file = TFile::Open("jethist.root","recreate");

  // Process selection.
  pythia.readFile("jet-model.cmnd");
  pythia.init();

  int nEvent   = pythia.mode("Main:numberOfEvents");

  // Jet definitions
  fjcore::JetDefinition jetDef(fjcore::antikt_algorithm, 0.4);
  std::vector <fjcore::PseudoJet> fjInputs;

  // Histograms.
  double jetimage[21][21];
  double rmax[21][21];

  TH2D hjetImage("JetImage", "Jet image", 100, -0.5, 0.5, 100, -1.0, 1.0);
  TH2D hrpt("RmaxPT", "Rmax versus pT of the jet", 100, 0, 1.0, 100, 0, 400.0);
  TH1D hjetpt("JetPT0", "pT of the hardest jet", 100, 0, 400);
  
  for (int i = 0; i <= 20; i++)
    for (int j = 0; j <= 20; j++){
      jetimage[i][j] = 0.0;
      rmax[i][j] = 0.0;
    }
 
  double pTsum = 0.0; // Overall normalisation for histogram
  int nSum = 0;

  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;

    // Begin FastJet analysis: extract particles from event record.

    fjInputs.resize(0);

    for (int i = 0; i < event.size(); ++i) if (event[i].isFinal()) {

      // Require visible/charged particles inside detector.
      if (!event[i].isVisible() ) continue;
      if (etaMax < 20. && abs(event[i].eta()) > etaMax) continue;

      // Create a PseudoJet from the complete Pythia particle.
      fjcore::PseudoJet particleTemp = fjcore::PseudoJet(event[i].px(),
							 event[i].py(),
							 event[i].pz(),
							 event[i].e());

      // Store acceptable particles as input to Fastjet.
      // Conversion to PseudoJet is performed automatically
      // with the help of the code in FastJet3.h.
      fjInputs.push_back( particleTemp);

    }

    // Run Fastjet algorithm and sort jets in pT order.
    vector <fjcore::PseudoJet> inclusiveJets, sortedJets;
    fjcore::ClusterSequence clustSeq(fjInputs, jetDef);
    inclusiveJets = clustSeq.inclusive_jets(pTMin);
    sortedJets    = sorted_by_pt(inclusiveJets);

    // 2D plot of constituents of the hardest jet

    if (int(sortedJets.size()) < 1) continue;

    nSum++;

    vector<fjcore::PseudoJet> constituents = sortedJets[0].constituents();
    double jetpt = sortedJets[0].pt();

    hjetpt.Fill(jetpt);
    
    pTsum += jetpt;
    
    double r_outsq = 0.0;
    for (int k = 0; k < int(constituents.size()); k++){
      // Loop to make jet image
      
      double zk = constituents[k].pt() / jetpt;

      double deta = constituents[k].eta() - sortedJets[0].eta();
      double dphi = constituents[k].phi() - sortedJets[0].phi();

      if (deta * deta + dphi * dphi > r_outsq) r_outsq = deta * deta + dphi * dphi;

      int ieta = floor( (deta + 0.5 ) * 20) ;
      if (ieta < 0) ieta = 0;
      if (ieta > 20) ieta = 20;

      int iphi = floor( (dphi/3.14 + 0.5 ) * 20);
      if (iphi < 0) iphi = 0;
      if (iphi > 20) iphi = 20;

      // cout << "k : " << k << " ieta: " << ieta << " iphi: " << iphi << endl;
      // cout << "zk: " << zk << " deta: " << deta << " dphi: " << dphi << endl;
      
      jetimage[ieta][iphi] += zk;
      hjetImage.Fill(deta,dphi,zk);
      
    }

    int irmax = floor( sqrt(r_outsq) * 20 ); if (irmax > 20) irmax = 20;
    int ipt = floor( jetpt / 20.0 ) ; // 0 - 400 GeV currently
    if (ipt > 20) ipt = 20;

    rmax[irmax][ipt] += 1.0;
    hrpt.Fill(sqrt(r_outsq), jetpt);

  // End of event loop.
  }


  // Write histograms in root file

  file->WriteObject(&hrpt, "RmaxPT");
  file->WriteObject(&hjetImage,"JetImage");
  file->WriteObject(&hjetpt, "JetPT0");
  file->Close();
  
  ofstream jetimg, rpt;
  jetimg.open("jetimage.dat");
  rpt.open("rpt.dat");

  // Normalise the jet image by number of events counted
  for (int i = 0; i <= 20; i++) {
    for (int j = 0; j <= 20; j++) {
      jetimage[i][j] /= nSum;

      cout << jetimage[i][j] << "  ";
      jetimg << jetimage[i][j] << "  ";
    }
    cout << endl;
    jetimg << endl;
  }
  
  // Jet width with pT
  
  for (int i = 0; i <= 20; i++) {
    for (int j = 0; j <= 20; j++) {
      rmax[i][j] /= nSum;

      cout << rmax[i][j] << "  ";
      rpt  << rmax[i][j] << "  ";
      
    }
    cout << endl;
    rpt << endl;
  }


  jetimg.close();
  rpt.close();

  // Statistics. Histograms.
  pythia.stat();


  // Done.
  return 0;
}


