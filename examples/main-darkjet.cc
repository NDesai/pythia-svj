
#include "Pythia8/Pythia.h"

using namespace Pythia8;

//==========================================================================

// Method to pick a number according to a Poissonian distribution.

int poisson(double nAvg, Rndm& rndm) {

  // Set maximum to avoid overflow.
  const int NMAX = 100;

  // Random number.
  double rPoisson = rndm.flat() * exp(nAvg);

  // Initialize.
  double rSum  = 0.;
  double rTerm = 1.;

  // Add to sum and check whether done.
  for (int i = 0; i < NMAX; ) {
    rSum += rTerm;
    if (rSum > rPoisson) return i;

    // Evaluate next term.
    ++i;
    rTerm *= nAvg / i;
  }

  // Emergency return.
  return NMAX;
}

//==========================================================================


int main() {

  
  // Generator. Shorthand for event.
  Pythia pythia, darkpythia;
  
  Event& event = pythia.process;
  Event& darkevent = darkpythia.event;
  
  // Allow full DS machinery for comparison
  pythia.readFile("HVbenchmark.cmnd");

  bool darkOnly = true;
  // Turn off showering and hadronisation if only doing the darkjet algo
  if(darkOnly){
    pythia.readstring("PartonLevel:ISR = off");
    pythia.readstring("PartonLevel:FSR = off");
    pythia.readstring("HadronLevel:all = off");
  }
  
  pythia.init();

  // Dark Pythia starts with the same "process" but then replaces the
  // dark quarks with dark meson jets directly
  // darkpythia.readstring();

  darkpythia.readstring("ProcessLevel:all = off");
  darkpythia.readString("Check:event = off");

  // Set dark hadron mass and branching fraction
  darkpythia.readstring("4900111:m0 = 10.0");
  darkpythia.readstring("4900211:m0 = 10.0"); 
  darkpythia.readstring("4900111:oneChannel = 1 1.0000000 91 4 -4");
  darkpythia.readstring("4900211:mayDecay = off");

  // Set parameters for dark jets
  double aparam = 0.1; 
  double rmax = 0.5;
  double r_inv = 0.5;  

  darkpythia.init();

  int nEvent   = pythia.mode("Main:numberOfEvents");

  int nAvg = 10; // Average number of hadrons

  
  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) { continue;}

    // Read process and add to darkevent
    darkevent.reset();
    // Append the entire hard process into the dark event
    darkevent = pythia.process;

    // Find number of hadrons to place in each jet
    int nHad1 = poisson(nAvg, darkpythia.rndm);
    int nHad2 = poisson(nAvg, darkpythia.rndm);
      
    


     
    // Shower and hadronise what remains
    if (!pythia.next()) continue;
    
  }
  event.list();
  // Statistics. Histograms.
  pythia.stat();


  // Done.
  return 0;
  
}


